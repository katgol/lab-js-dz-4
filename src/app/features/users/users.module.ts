import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserService } from 'src/app/core/services/user/user.service';

@NgModule({
  declarations: [
    UsersComponent,
  ],
  imports: [
    SharedModule,
    UsersRoutingModule,
  ],
  providers: [UserService]
})
export class UsersModule { }

